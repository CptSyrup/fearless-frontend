window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/locations/";
  const response = await fetch(url);

  const data = await response.json();

  const locationSelect = document.getElementById("location");
  for (let location of data.locations) {
    locationSelect.innerHTML += `<option value="${location.id}">${location.name}</option>`;
  }

  const formTag = document.getElementById("create-conference-form");
  formTag.addEventListener("submit", async (event) => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);
    const locationURL = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationURL, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
      console.log(newLocation);
    }
  });
});
