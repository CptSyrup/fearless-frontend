function createCard(
  name,
  description,
  pictureUrl,
  startdate,
  enddate,
  location
) {
  return `
    <div class="col gy-4">
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${startdate} - ${enddate}
        </div>
      </div>
    </div>
    `;
}

function createError() {
  return `
  <div class="alert alert-primary" role="alert">
        A simple primary alert—check it out!
  </div>
  `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      const container = document.querySelector(".container");
      container.innerHTML = createError();
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const location = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startdate = new Date(
            details.conference.starts
          ).toLocaleDateString();
          const enddate = new Date(
            details.conference.ends
          ).toLocaleDateString();
          const html = createCard(
            title,
            description,
            pictureUrl,
            startdate,
            enddate,
            location
          );
          const column = document.querySelector(".row");
          column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    const container = document.querySelector(".container");
    container.innerHTML = createError();
  }
});
