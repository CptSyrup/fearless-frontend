window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/states/";
  const response = await fetch(url);

  //if (response.ok) {
  const data = await response.json();
  console.log(data);

  const stateSelect = document.getElementById("state");
  console.log(state);
  for (let state of data.states) {
    stateSelect.innerHTML += `<option value="${state.abbreviation}">${state.name}</option>`;
  }
  //}
  const formTag = document.getElementById("create-location-form");
  formTag.addEventListener("submit", async (event) => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);
    const locationURL = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationURL, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
      console.log(newLocation);
    }
  });
});
